import 'package:get/get.dart';
// import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:flutter/material.dart';
import 'package:prayer_timetable_flutter/store/state.dart';
import 'package:prayer_timetable_flutter/theme/painter_clock.dart';

class Seconds extends StatefulWidget {
  const Seconds({Key? key}) : super(key: key);

  @override
  _SecondsState createState() => _SecondsState();
}

class _SecondsState extends State<Seconds> {
  // Define the various properties with default values. Update these properties
  // when the user taps a FloatingActionButton.

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => CustomPaint(
        painter: ClockPainter(
            stateStore.time.second.toDouble(), stateStore.time.minute % 2 == 0),
      ),
    );
  }
}
