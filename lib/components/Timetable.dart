// import dart and flutter resources
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

// import package resources
// import 'package:flutter_vector_icons/flutter_vector_icons.dart';

// import local package resources
import 'package:prayer_timetable_flutter/theme/colors.dart';
import 'package:prayer_timetable_flutter/theme/text.dart';

import 'package:prayer_timetable_flutter/store/prayer.dart';
import 'package:prayer_timetable_flutter/store/state.dart';
// import 'package:prayer_timetable/prayer_timetable.dart';

class Timetable extends StatelessWidget {
  const Timetable({Key? key}) : super(key: key);

  //def
  final bool isNext = false;

  @override
  Widget build(BuildContext context) {
    // if (!stateStore.showCompass) stateStore.compass?.cancel();

    // stateStore.compassClose();

    return SizedBox(
      // color: colorBlackTrans,
      height: 190.h,
      // padding: EdgeInsets.symmetric(horizontal: 32.w, vertical: 56.h),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        // crossAxisAlignment: CrossAxisAlignment.center,
        // mainAxisSize: MainAxisSize.max,
        children: [
          Obx(() => getPrayer(0)),
          Obx(() => getPrayer(1)),
          Obx(() => getPrayer(2)),
          Obx(() => getPrayer(3)),
          Obx(() => getPrayer(4)),
          Obx(() => getPrayer(5)),
          // Divider(
          //   color: colorBlackTransLite,
          //   height: 1.h,
          // )
        ],
      ),
    );
  }
}

Widget getPrayer(int no) {
  bool isNext = prayerStore.isJamaahPending
      ? prayerStore.currentId == no
      : prayerStore.nextId == no;

  return Column(
    children: [
      Container(
        width: 275.w,
        height: 120.h,
        // color: colorPrimaryLite,
        decoration: BoxDecoration(
          color: prayerStore.currentId < no ? colorPrimaryLite : colorPrimary,
          border: Border.all(
            color: isNext
                ? colorSecondary
                : prayerStore.currentId < no
                    ? colorPrimaryLite
                    : colorPrimary,
            width: 5.w,
          ),
          // borderRadius: BorderRadius.circular(5.w),
        ),

        // BOX
        child: SizedBox(
          // height: 120.h,
          // color: colorPrimaryLite,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              // spacer
              // SizedBox(height: 10.h),

              // PRAYER NAME
              SizedBox(
                  // color: colorSecondaryTrans,
                  child: TextBody(
                      prayerStore.strings.prayerNames[no].toUpperCase())),

              // PRAYER TIMES
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  SizedBox(
                    child: TextBody(
                      prayerStore.strings.prayers[no],
                      align: prayerStore.jamaahOn
                          ? TextAlign.center
                          : TextAlign.right,
                      weight: isNext && !prayerStore.isJamaahPending
                          ? FontWeight.w700
                          : FontWeight.w400,
                    ),
                  ),
                  SizedBox(
                    child: TextBody(
                      prayerStore.strings.jamaah[no],
                      align: TextAlign.right,
                      weight: isNext && prayerStore.isJamaahPending
                          ? FontWeight.w700
                          : FontWeight.w400,
                    ),
                  ),
                ],
              ),

              // spacer
              // SizedBox(height: 2.h),
            ],
          ),
        ),
      ),

      SizedBox(height: 10.h),

      // BAR
      Stack(
        children: [
          // Background
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: no < prayerStore.nextId
                      ? colorSecondary
                      : colorWhiteTrans,
                  border: Border.all(
                    color: Colors.transparent,
                    width: 0,
                  ),
                  // borderRadius: BorderRadius.circular(5.w),
                ),
                width: 275.w,
                height: 60.h,
                child: isNext ? countdown() : const SizedBox(),
              ),
            ],
          ),
          // Progress
          if (isNext)
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  alignment: Alignment.bottomCenter,
                  decoration: BoxDecoration(
                    color: colorSecondary,
                    border: Border.all(
                      color: Colors.transparent,
                      width: 0,
                    ),
                    // borderRadius: BorderRadius.circular(5.w),
                  ),
                  width: 275.w * prayerStore.calc.percentage / 100,
                  height: 60.h,
                ),
              ],
            ),
        ],
      ),
    ],
  );
}

Widget countdown() => FittedBox(
    fit: BoxFit.scaleDown,
    child: TextBody(
      !prayerStore.isJamaahPending &&
              stateStore.time.isBefore(
                  prayerStore.currentTime.add(const Duration(minutes: 5)))
          ? 'NOW'
          : prayerStore.strings.countdownTime,
      size: 64.sp,
      color: !prayerStore.isJamaahPending &&
              stateStore.time.isBefore(
                  prayerStore.currentTime.add(const Duration(minutes: 5)))
          ? Colors.red
          : colorText,
    ));
