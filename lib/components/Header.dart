import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';

// import local package resources

import 'package:prayer_timetable_flutter/theme/colors.dart';
import 'package:prayer_timetable_flutter/theme/text.dart';

// import 'package:prayer_timetable_flutter/components/parts/Date.dart';
// import 'package:prayer_timetable_flutter/components/parts/Countdown.dart';

import 'package:prayer_timetable_flutter/store/prayer.dart';

class Header extends StatelessWidget {
  const Header({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const String assetName = 'assets/config/logo.svg';
    final Widget svgIcon = SvgPicture.asset(assetName,
        color: colorTextLight, semanticsLabel: 'Logo');

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          alignment: Alignment.center,
          padding: EdgeInsets.symmetric(horizontal: 20.sp, vertical: 20.sp),
          height: 80.h,
          color: colorHeader,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                  alignment: Alignment.centerLeft,
                  // color: colorBlackTrans,
                  width: 360.w,
                  child: const Date()),

              // divider
              // VerticalDivider(
              //   width: 30.w,
              //   thickness: 2.w,
              //   color: colorBlackTrans,
              // ),

              svgIcon,

              // const Expanded(child: Countdown()),
              Container(
                  alignment: Alignment.centerRight,
                  // color: colorBlackTrans,
                  width: 360.w,
                  child: const Hijri()),
            ],
          ),
        ),
      ],
    );
  }
}

// Date
class Date extends StatelessWidget {
  const Date({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        FittedBox(
          fit: BoxFit.scaleDown,
          child: TextBody(
            prayerStore.strings.date,
            size: 32,
          ),
        ),
      ],
    );
  }
}

// Date
class Hijri extends StatelessWidget {
  const Hijri({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        FittedBox(
            fit: BoxFit.scaleDown,
            child: TextBody(
              prayerStore.strings.hijri,
              size: 32,
            )),
      ],
    );
  }
}
