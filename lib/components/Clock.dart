// import dart and flutter resources
import 'dart:ui' as ui;
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:animated_flip_counter/animated_flip_counter.dart';

// import local package resources
import 'package:prayer_timetable_flutter/theme/colors.dart';

import 'package:prayer_timetable_flutter/store/state.dart';
import 'package:prayer_timetable_flutter/components/Seconds.dart';

class Clock extends StatelessWidget {
  const Clock({Key? key, lite}) : super(key: key);

  // Clock(
  //   this.lite,
  // );
  //def
  final bool lite = false;
  @override
  Widget build(BuildContext context) => Obx(() => Stack(
        // mainAxisAlignment: MainAxisAlignment.center,
        // crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
              // alignment: Alignment.center,
              width: 980.w,
              height: 360.h,
              child: const Seconds()),
          Container(
            // color: colorBlackTrans,
            padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 20.h),
            //   // margin: EdgeInsets.symmetric(vertical: 12),
            width: 980.w,
            height: 380.h,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              // crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                //       // if ( stateStore.time.second < 10) Text('0'),
                // hour
                digit(stateStore.time.hour),
                // dots
                AnimatedOpacity(
                    opacity: stateStore.separatorVisible ? 1.0 : 0.0,
                    duration: const Duration(milliseconds: 150),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          width: 30.sp,
                          height: 30.sp,
                          color: colorTextLight,
                        ),
                        SizedBox(height: 30.sp),
                        Container(
                          width: 30.sp,
                          height: 30.sp,
                          color: colorTextLight,
                        )
                        // TextClock(':', color: colorWhiteTransStrong),
                      ],
                    )),
                // minute
                digit(stateStore.time.minute),
              ],
            ),
          ),
        ],
      ));
}

Widget digit(int value) => Stack(
      alignment: Alignment.center,
      children: [
        // border(40),
        Container(
          // alignment: Alignment.center,
          width: 440.w,
          height: 340.h,
          // margin: EdgeInsets.fromLTRB(24, 40, 24, 0.w),
          padding: EdgeInsets.fromLTRB(
              GetPlatform.isLinux ? 35.w : 0.w, 0.h, 0.w, 0.h),
          // color: colorTextLight,
          // color: colorBlackTransLite,
          // decoration: BoxDecoration(
          //     border: Border.all(
          //       color: colorWhiteTrans,
          //       width: 4.w,
          //     ),
          //     gradient: LinearGradient(
          //       begin: Alignment.topCenter,
          //       end: Alignment.bottomCenter,
          //       colors: [
          //         colorBlackTrans,
          //         colorWhiteTrans,
          //         colorBlackTrans,
          //       ],
          //       stops: const [
          //         0.1,
          //         0.5,
          //         0.9,
          //       ],
          //     )),
        ),
        Positioned(
          top: -0.h,
          child: AnimatedFlipCounter(
            mainAxisAlignment: MainAxisAlignment.center,
            curve: Curves.bounceInOut,
            value: value.toDouble(),
            wholeDigits: 2,
            textStyle: TextStyle(
              fontSize: 340.w,
              // color: colorWhiteTransStrong,
              fontFamily: 'SourceSansPro',
              fontWeight: FontWeight.bold,
              height: 1,
              //  textBaseline: ui.TextBaseline.alphabetic,
              decoration: TextDecoration.none,
              letterSpacing: 30.w,

              // color: colorText,
              foreground: Paint()
                ..style = PaintingStyle.fill
                ..strokeWidth = 8.w
                ..color = Colors.black
                ..shader = ui.Gradient.linear(
                  Offset(20.w, 150.h),
                  Offset(40.w, 200.h),
                  <Color>[
                    colorHeader,
                    colorBackground,
                    Colors.redAccent,
                    Colors.grey,
                    colorClock,
                  ],
                  [
                    0.0,
                    0.499,
                    0.5,
                    0.53,
                    1.0,
                  ],
                ),

              // background: Paint()
              // ..style = PaintingStyle.stroke
              // ..strokeWidth = 2.w
              // ..color = Colors.blue[700]!
              //   ..shader = ui.Gradient.linear(
              // const Offset(0, 20),
              // const Offset(150, 20),
              // <Color>[
              //   Colors.red,
              //   Colors.yellow,
              // ],
              //   ),

              // prefix: stateStore.time.second < 10 ? '0' : '',
            ),
          ),
        )
        // border(20),
      ],
    );

Widget border(int position) => Container(
    width: 8.w,
    height: double.infinity,
    // padding: EdgeInsets.all(32.w),
    decoration: BoxDecoration(
        //     border: Border.all(
        //   color: colorWhiteTrans,
        //   width: 2,
        // ),
        gradient: LinearGradient(
      begin: Alignment.topCenter,
      end: Alignment.bottomCenter,
      colors: [
        colorWhiteTrans.withAlpha(20),
        colorWhiteTrans,
        colorWhiteTrans.withAlpha(20),
      ],
      stops: const [
        0.1,
        0.5,
        0.9,
      ],
    )));
