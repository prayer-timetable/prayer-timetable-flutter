import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:prayer_timetable_flutter/components/Seconds.dart';

import 'package:prayer_timetable_flutter/theme/colors.dart';
import 'package:prayer_timetable_flutter/components/Clock.dart';
import 'package:prayer_timetable_flutter/components/Header.dart';
import 'package:prayer_timetable_flutter/components/Footer.dart';
import 'package:prayer_timetable_flutter/components/Timetable.dart';

class Main extends StatelessWidget {
  const Main({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      // padding: EdgeInsets.fromLTRB(20.h, 20.h, 0, 0),
      width: 1080.w,
      height: 1920.h,
      color: colorPrimary,
      child: Stack(
        // alignment: AlignmentDirectional.topCenter,
        alignment: Alignment.center,
        children: [
          Container(
            color: colorBackground,
            // padding: EdgeInsets.all(70.sp),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Header(),
                // divider
                // Divider(
                //   height: 30.h,
                //   thickness: 4.w,
                //   color: colorBlackTrans,
                // ),
                const Clock(),
                // Timetable(),
                const Footer(),
              ],
            ),
          ),

          // const Seconds()
        ],
      ),
    );
  }
}
