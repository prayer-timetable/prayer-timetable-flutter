import 'package:flutter/material.dart';
import 'package:prayer_timetable_flutter/theme/colors.dart';

final ButtonStyle flatButtonStyle = TextButton.styleFrom(
  primary: Colors.black87,
  minimumSize: const Size(439, 70),
  padding: const EdgeInsets.symmetric(horizontal: 0.0, vertical: 0.0),
  shape: const RoundedRectangleBorder(
    borderRadius: BorderRadius.all(Radius.circular(2.0)),
  ),
);

final ButtonStyle flatButtonCircleStyle = TextButton.styleFrom(
  primary: Colors.black87,
  minimumSize: const Size(170, 170),
  padding: const EdgeInsets.symmetric(horizontal: 0.0, vertical: 0.0),
  shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(170)),
      side: BorderSide(color: Colors.transparent, width: 20)),
);

ButtonStyle tileButtonStyle(
        {bool xl = false, Color colorBg = Colors.black87}) =>
    ElevatedButton.styleFrom(
      padding: EdgeInsets.symmetric(horizontal: xl ? 16 : 4, vertical: 4),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
      primary: colorBg,
      minimumSize: const Size(110, 70),
    );

ButtonStyle roundButtonStyle = ElevatedButton.styleFrom(
  padding: const EdgeInsets.all(4),
  primary: colorPrimary,
  shape: const CircleBorder(),
  // onPrimary: colorHighlight,
  // onSurface: colorHighlight,
  // shadowColor: colorHighlight,

  // splashColor: colorHighlight, // inkwell color
);
