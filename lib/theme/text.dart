import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:prayer_timetable_flutter/theme/colors.dart';

class TextBody extends StatelessWidget {
  final String data;
  final TextAlign align;
  final TextStyle style;
  final bool card;
  final Color color;
  final double height;
  final double size;
  final FontWeight weight;

  TextBody(
    this.data, {
    Key? key,
    this.align = TextAlign.justify,
    this.height = 1,
    this.card = false,
    this.size = 24,
    this.weight = FontWeight.normal,
    TextStyle style = const TextStyle(),
    Color color = colorText,
  })  : color = color,
        style = style.copyWith(
          fontFamily: 'SourceSansPro',
          fontSize: size != 24 ? size.sp : 24.sp,
          height: height,
          color: color,
        ),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      data,
      textAlign: align,
      style: style.copyWith(
        color: color,
        fontWeight: weight != FontWeight.w400 ? weight : FontWeight.w400,
      ),
      softWrap: card ? false : true,
      maxLines: card ? 2 : 1000,
      overflow: card ? TextOverflow.ellipsis : TextOverflow.fade,
    );
  }
}

class TextClock extends StatelessWidget {
  final String data;
  final TextAlign align;
  final TextStyle style;
  final Color color;

  TextClock(
    this.data, {
    Key? key,
    this.align = TextAlign.center,
    TextStyle style = const TextStyle(),
    Color color = colorText,
  })  : color = color,
        style = style.copyWith(
          fontFamily: 'SourceSansPro',
          fontSize: 480.sp,
          height: 1,
        ),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return FittedBox(
      fit: BoxFit.scaleDown,
      child: Text(
        data,
        textAlign: align,
        style: style.apply(color: color),
      ),
    );
  }
}
