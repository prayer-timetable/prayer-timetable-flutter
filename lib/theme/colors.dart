// import dart and flutter resources
import 'package:flutter/material.dart';

bool liteTheme = true;
// **********
// THEME 1
// **********
// main
const Color colorBackground = Color(0xff233542);
const Color colorText = Color(0xffe9e9e9); // #bfc0c0
const Color colorHeader = Color.fromARGB(255, 87, 77, 24);
const Color colorFooter = Color.fromARGB(255, 77, 32, 62);

const Color colorClock = Color.fromARGB(255, 255, 166, 84); // #bfc0c0

//////
const Color colorPrimaryDark = Color(0xff233542); // #233542
const Color colorPrimary = Color(0xff334451); // #334451
const Color colorPrimaryLite = Color(0xff566774); // #566774

const Color colorSecondary = Color.fromARGB(255, 137, 75, 44); // #dd7744
const Color colorHighlight = Color(0xff99bbbb); // #ff686b

// text
const Color colorTextLight = Color(0xffe3e3e3); // #e3e3e3

// **********
// THEME 2
// **********
// // main
// Color colorPrimary = Color(0xff250902); // #250902
// Color colorSecondary = Color(0xffAD2831); // #AD2831
// Color colorHighlight = Color(0xffDD6E42); // #DD6E42

// // text
// Color colorText = Color(0xff11151C); // #11151C
// Color colorTextLight = Color(0xffFAF9F9); // #FAF9F9

// transparent
Color colorWhiteTrans = const Color(0x22ffffff);
Color colorWhiteTransStrong = const Color(0x99ffffff);
Color colorWhiteTransStronger = const Color(0xddffffff);
Color colorBlackTrans = const Color(0x33000000);
Color colorGrayTransLite = const Color(0xaaeeeeee);
Color colorBlackTransLite = const Color(0x11000000);
Color colorBlackTransDark = const Color(0xaa000000);
Color colorPrimaryTrans = const Color(0x77375267); // #253443
Color colorSecondaryTrans = const Color(0x77CC6633); // #CC6633
