import 'package:flutter/material.dart';

Widget eventlistWidget(List events, String splitter) {
  return ListView.builder(
      itemCount: events.length,
      itemBuilder: (BuildContext context, int index) {
        List<String> event = '${events[index]}'.split(splitter);
        return InputDecorator(
            decoration: InputDecoration(
                contentPadding:
                    const EdgeInsets.only(left: 5, top: 5, bottom: 5),
                labelStyle: const TextStyle(color: Colors.blue, fontSize: 16.0),
                labelText: '[${event[0].toString()}]'),
            child: Text(event[1],
                style: const TextStyle(color: Colors.black, fontSize: 16.0)));
      });
}
