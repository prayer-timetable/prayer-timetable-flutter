import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:prayer_timetable_flutter/theme/colors.dart';

Widget imageWidget(String url, {bool fill = false}) {
  // print('url: $url');
  // return Image.network('https://picsum.photos/250?image=9');
  // return Image.network(url);
  return CachedNetworkImage(
    // imageUrl: "http://via.placeholder.com/350x150",
    imageUrl: url,
    imageBuilder: (context, imageProvider) => Container(
      decoration: BoxDecoration(
        image: DecorationImage(
            image: imageProvider,
            fit: fill ? BoxFit.fill : BoxFit.cover,
            colorFilter:
                ColorFilter.mode(colorBlackTrans, BlendMode.colorBurn)),
      ),
    ),
    progressIndicatorBuilder: (context, url, downloadProgress) =>
        CircularProgressIndicator(value: downloadProgress.progress),
    errorWidget: (context, url, error) => const Icon(Icons.error),
  );
  // return CachedNetworkImage(
  //   useOldImageOnUrlChange: true,
  //   imageUrl: url,
  //   imageBuilder: (context, imageProvider) => Container(
  //     decoration: BoxDecoration(
  //       image: DecorationImage(
  //         image: imageProvider,
  //         fit: fill ? BoxFit.fill : BoxFit.cover,
  //         // colorFilter: ColorFilter.mode(colorOrange, BlendMode.screen),
  //       ),
  //     ),
  //   ),
  //   placeholder: (context, url) => Center(child: CircularProgressIndicator()),
  //   errorWidget: (context, url, error) => Icon(Icons.error),
  // );
}
