// import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

// import 'package:flutter_html/shims/dart_ui_real.dart';
import 'package:prayer_timetable_flutter/theme/colors.dart';
// mobx

class ClockPainter extends CustomPainter {
  Color color1 = colorPrimary;
  Color color2 = colorPrimaryLite;

  Paint _paint1 = Paint();
  // Paint _paint2;
  Paint _paint2 = Paint();
  Paint _paint3 = Paint();
  double seconds = 0;
  double stroke = 20.sp;
  // Paint _paint0;

  ClockPainter(double seconds, even) {
    this.seconds = seconds / 60 * 100;

    // _paint1 = Paint()
    //   ..color = colorBlackTrans
    //   ..style = PaintingStyle.stroke
    //   ..strokeWidth = stroke
    //   ..maskFilter = MaskFilter.blur(BlurStyle.outer, 3.0);

    // _paint2 = Paint()
    //   ..color = colorBlackTrans
    //   // ..strokeWidth = 8.0
    //   ..style = PaintingStyle.fill;

    _paint1 = Paint()
      ..color = even ? color1 : color2
      ..strokeWidth = stroke
      ..strokeJoin = StrokeJoin.miter
      // ..strokeCap = StrokeCap.square
      ..style = PaintingStyle.stroke;

    _paint2 = Paint()
      ..color = even ? color2 : color1
      ..strokeWidth = stroke
      ..strokeJoin = StrokeJoin.miter
      // ..strokeCap = StrokeCap.square
      ..style = PaintingStyle.stroke;

    _paint3 = Paint()
      ..color = colorBlackTransDark
      ..strokeWidth = stroke
      ..strokeJoin = StrokeJoin.miter
      // ..strokeCap = StrokeCap.square
      ..maskFilter = MaskFilter.blur(BlurStyle.normal, 8.sp)
      ..style = PaintingStyle.stroke;

    // _paint0 = Paint()
    //   ..color = colorBlackTrans
    //   // ..strokeWidth = 8.0
    //   ..style = PaintingStyle.fill;
  }

  @override
  void paint(Canvas canvas, Size size) {
    // final angle = 2 * pi / 16;
    final width = size.width - stroke;
    final height = size.height - stroke; // 120/160
    // final thickness = 5.0;
    // final offset = 2 * thickness;

    final rect = Rect.fromLTRB(0 + stroke / 2, 0 + stroke / 2,
        width + stroke / 2, height + stroke / 2);
    // final startAngle = -pi / 2;
    // final sweepAngle = 2 * pi;
    // final sweepAngleseconds = 2 * pi / 100 * seconds;
    // final useCenter = false;

    // drawing
    // canvas.translate(0 + 2 * thickness, width / 3 + 2 * thickness);

    // bg
    // canvas.drawCircle(Offset(0.0, 0.0), width, _paint3);
    // canvas.drawRect(rect, _paint2);

    // shadow
    canvas.drawRect(rect, _paint3);

    canvas.drawRect(rect, _paint2);

    Path path = Path();
    path.moveTo(size.width / 2, stroke / 2);
    path.lineTo(size.width - stroke / 2, stroke / 2);
    path.lineTo(size.width - stroke / 2, size.height - stroke / 2);
    path.lineTo(stroke / 2, size.height - stroke / 2);
    path.lineTo(stroke / 2, stroke / 2);
    path.lineTo(size.width / 2, stroke / 2);

    final pathMetric = path.computeMetrics().first;
    final subPath =
        pathMetric.extractPath(0, pathMetric.length * seconds / 100);

    // path.lineTo(size.width, size.height);
    canvas.drawPath(subPath, _paint1);

// path.pa()

    // // fill
    // canvas.drawArc(rect, startAngle, sweepAngle, useCenter, _paint2);
    // // stroke
    // canvas.drawArc(rect, startAngle, sweepAngle, useCenter, _paint3);
    // // seconds
    // canvas.drawArc(rect, startAngle, sweepAngleseconds, useCenter, _paint4);

    // pointer circle
    // canvas.drawCircle(Offset(0.0, 0.0), thickness, _paint3);

    // canvas.rotate(angle);

    canvas.save();
    canvas.restore();
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
