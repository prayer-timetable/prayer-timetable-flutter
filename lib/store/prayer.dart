import 'package:flutter/foundation.dart';

import 'package:prayer_timetable/prayer_timetable.dart';
import 'package:hijri/hijri_calendar.dart';
import 'package:date_format/date_format.dart';

import 'package:get/get.dart';

import 'package:prayer_timetable_flutter/store/class.dart';
import 'package:prayer_timetable_flutter/store/res/timetable.dart';
import 'package:prayer_timetable_flutter/store/state.dart';
import 'package:prayer_timetable_flutter/store/api.dart';
import 'package:prayer_timetable_flutter/store/res/helpers.dart';

// Instantiate your class using Get.put() to make it available for all "child" routes there.
final PrayerController prayerStore = Get.put(PrayerController());

class PrayerController extends GetxController {
  // ********** LIFECYCLE **********
  @override
  void onInit() {
    if (kDebugMode) print('prayer init');
    // fetchApi();
    getPrayers(0);
    super.onInit();
  }

  @override
  void onClose() {
    if (kDebugMode) print('prayer close');
    // user.close();
    // name.close();
    super.onClose();
  }

  // ********** OBSERVABLES **********
  final stringsObs = defaultStrings.obs;
  var nextIdObs = 0.obs;
  var currentTimeObs = DateTime.now().obs;
  var nextTimeObs = DateTime.now().obs;

  // ********** GETTERS **********
  Strings get strings => stringsObs.value;
  int get nextId => nextIdObs.value;
  DateTime get currentTime => currentTimeObs.value;
  DateTime get nextTime => nextTimeObs.value;

  // ********** INITIALISERS **********
  bool countownMode = true;

  Prayers prayers = Prayers();

  Prayers prayersTomorrow = Prayers();

  bool isJamaahPending = false;

  Calc calc = Calc(
      DateTime.now(),
      Prayers(),
      Prayers(),
      Prayers(),
      true,
      Jamaah(Prayers(), ['', '', '', '', '', ''], [[], [], [], [], [], []]),
      Jamaah(Prayers(), ['', '', '', '', '', ''], [[], [], [], [], [], []]),
      Jamaah(Prayers(), ['', '', '', '', '', ''], [[], [], [], [], [], []]),
      0,
      0);

  bool prayerLoaded = false;

  int fajrTimeStamp = (stateStore.time.millisecondsSinceEpoch / 1000).floor();

  int sunriseTimeStamp =
      (stateStore.time.millisecondsSinceEpoch / 1000).floor();

  int dhuhrTimeStamp = (stateStore.time.millisecondsSinceEpoch / 1000).floor();

  int asrTimeStamp = (stateStore.time.millisecondsSinceEpoch / 1000).floor();

  int maghribTimeStamp =
      (stateStore.time.millisecondsSinceEpoch / 1000).floor();

  int ishaTimeStamp = (stateStore.time.millisecondsSinceEpoch / 1000).floor();

  bool jamaahOn = true;

  int currentId = 0;

  // ********** FUNCTIONS **********
  void getPrayers(int sec) {
    PrayerTimetableMap _location;

    _location = PrayerTimetableMap(
      timetableDublin,
      jamaahOn: jamaahOn,
      jamaahMethods: apiStore.jamaahMethods,
      jamaahOffsets: apiStore.jamaahOffsets,
      // optional parameters:
      year: stateStore.time.year,
      month: stateStore.time.month,
      day: stateStore.time.day,
      hour: stateStore.time.hour,
      minute: stateStore.time.minute,
      second: stateStore.time.second,
      hijriOffset: 0,
      timezone: 'Europe/Dublin',
      joinDhuhr: apiStore.joinDhuhr,
      joinMaghrib: apiStore.joinMaghrib,
    );

    // print(apiStore.jamaahOffsets);

    Duration _countdown =
        _location.calc!.countDown + const Duration(seconds: 0);
    // print('${_location.calc.countDown} ${_location.calc.time}');
    Duration _countup = _location.calc!.countUp;
    int _currentId = _location.calc!.currentId;
    int _nextId = _location.calc!.nextId;

    // set next time
    currentTimeObs.value = _location.calc!.current;
    nextTimeObs.value = _location.calc!.next;

    String _currentName;
    String _nextName;

    if (_currentId == 0) {
      _currentName = 'fajr';
      _nextName = 'sunrise';
    } else if (_currentId == 1) {
      _currentName = 'sunrise';
      _nextName = 'dhuhr';
    } else if (_currentId == 2) {
      _currentName = 'dhuhr';
      _nextName = 'asr';
    } else if (_currentId == 3) {
      _currentName = 'asr';
      _nextName = 'maghrib';
    } else if (_currentId == 4) {
      _currentName = 'maghrib';
      _nextName = 'isha';
    } else if (_currentId == 5) {
      _currentName = 'isha';
      _nextName = 'fajr';
    } else {
      _currentName = 'isha';
      _nextName = 'fajr';
    }

    var _hDate = HijriCalendar.fromDate(stateStore.time);

    Prayers? _prayersToday = _location.prayers!.current;
    Prayers? _prayersTomorrow = _location.prayers!.next;

    Prayers? _jamaahToday = _location.jamaah!.current;
    Prayers? _jamaahTomorrow = _location.jamaah!.next;

    Prayers? _prayers =
        _location.calc!.isAfterIsha ? _prayersTomorrow : _prayersToday;

    Prayers? _jamaah =
        _location.calc!.isAfterIsha ? _jamaahTomorrow : _jamaahToday;

    isJamaahPending = _location.calc!.jamaahPending;

    stringsObs.value = Strings(
      // 'updatedString':
      //     '${formatDate(_settings.updated, [d, M, H, ':', nn, ':', ss])}',
      time: formatDate(stateStore.time, [H, ':', nn, ':', ss]),
      date: formatDate(
        stateStore.time,
        [DD, ' ', d, ' ', MM, ' ', yyyy],
      ),
      hijri: _hDate.toFormat('d MMMM yyyy'),
      hijriMonth: _hDate.toFormat('MMMM'),

      countdownName: capitalise(
          '${isJamaahPending ? _currentName : _nextName}${isJamaahPending ? " Iqamah" : ""}'), //%$_percentage
      countdownTime: printDuration(_countdown), //%$_percentage
      countupName: capitalise(_currentName), //%$_percentage
      countupTime: printDuration(_countup), //%$_percentage

      prayers: [
        formatDate(_prayers!.dawn, [H, ':', nn]),
        formatDate(_prayers.sunrise, [H, ':', nn]),
        formatDate(_prayers.midday, [H, ':', nn]),
        apiStore.joinDhuhr
            ? 'After'
            : formatDate(_prayers.afternoon, [H, ':', nn]),
        formatDate(_prayers.sunset, [H, ':', nn]),
        apiStore.joinMaghrib
            ? 'After'
            : formatDate(_prayers.dusk, [H, ':', nn]),
      ],
      jamaah: [
        formatDate(_jamaah!.dawn, [H, ':', nn]),
        '',
        formatDate(_jamaah.midday, [H, ':', nn]),
        apiStore.joinDhuhr
            ? 'Dhuhr'
            : formatDate(_jamaah.afternoon, [H, ':', nn]),
        formatDate(_jamaah.sunset, [H, ':', nn]),
        apiStore.joinMaghrib
            ? 'Maghrib'
            : formatDate(_jamaah.dusk, [H, ':', nn]),
      ],

      // 'jamaahStrings': [
      //   formatDate(_prayersList[0]['jtime'], [H, ':', nn]),
      //   formatDate(_prayersList[1]['jtime'], [H, ':', nn]),
      //   formatDate(_prayersList[2]['jtime'], [H, ':', nn]),
      //   formatDate(_prayersList[3]['jtime'], [H, ':', nn]),
      //   formatDate(_prayersList[4]['jtime'], [H, ':', nn]),
      //   formatDate(_prayersList[5]['jtime'], [H, ':', nn]),
      // ],
      current:
          "${capitalise(_currentName)} ${formatDate(_location.calc!.current, [
            H,
            ':',
            nn
          ])}",
      lastThird: formatDate(_location.sunnah!.lastThird, [H, ':', nn]),
      prayerNames: ['fajr', 'shurooq', 'dhuhr', 'asr', 'maghrib', 'isha'],
    );

    bool _prayerLoaded = true;

    // print(_location.calc!.nextId);
    // print(gotOnlineSettings);
    // cityMap = newcityMap;
    // return {
    //   'strings': strings,
    //   'prayerLoaded': prayerLoaded,
    //   'gotOnlineSettings': gotOnlineSettings,
    //   'prayers': prayers,
    // };

    prayers = _prayers;
    prayersTomorrow = _prayersTomorrow!;
    prayerLoaded = _prayerLoaded;
    calc = _location.calc!;

    currentId = _currentId;
    nextIdObs.value = _nextId;

    // if (stateStore.platform == PlatformType.desktop) {
    //   fajrTimeStamp = (prayers.dawn.millisecondsSinceEpoch / 1000).floor();
    //   sunriseTimeStamp =
    //       (prayers.sunrise.millisecondsSinceEpoch / 1000).floor();
    //   dhuhrTimeStamp = (prayers.midday.millisecondsSinceEpoch / 1000).floor();
    //   asrTimeStamp = (prayers.afternoon.millisecondsSinceEpoch / 1000).floor();
    //   maghribTimeStamp = (prayers.sunset.millisecondsSinceEpoch / 1000).floor();
    //   ishaTimeStamp = (prayers.dusk.millisecondsSinceEpoch / 1000).floor();
    // }
    // // test
    // prayerList = [_prayers.dawn,_prayers.sunrise, _prayers.midday];
    //? settings = _settings;
  }
}
