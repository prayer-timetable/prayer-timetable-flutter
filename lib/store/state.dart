import 'dart:async';
import 'package:flutter/foundation.dart';

import 'package:get/get.dart';
import 'package:prayer_timetable_flutter/store/prayer.dart';
import 'package:prayer_timetable_flutter/store/api.dart';
import 'package:prayer_timetable_flutter/store/settings.dart';

// Instantiate your class using Get.put() to make it available for all "child" routes there.
final MainController stateStore = Get.put(MainController());

class MainController extends GetxController {
  // ********** LIFECYCLE **********
  @override
  void onInit() {
    // uncomment to enable test time
    // test = true;
    if (test) timeObs.value = DateTime(DateTime.now().year, 4, 19, 13, 49, 45);

    tickTimer = Timer.periodic(const Duration(seconds: 1), (Timer t) => tick());
    periodicalTimer =
        Timer.periodic(const Duration(minutes: 60), (Timer t) => periodical());

    settingsStore.getLocal();
    // tick();
    // periodical();

    if (kDebugMode) print('state init');
    // fetchApi();
    super.onInit();
  }

  @override
  void onClose() {
    if (kDebugMode) print('state close');
    tickTimer.cancel();
    periodicalTimer.cancel();
    // user.close();
    // name.close();
    super.onClose();
  }

  // ********** OBSERVABLES **********
  var countObs = 0.obs;
  var timeObs = DateTime.now().obs;
  // img, title, summary, date, url
  var separatorVisibleObs = true.obs;

  // ********** GETTERS **********
  int get count => countObs.value;
  DateTime get time => timeObs.value;
  bool get separatorVisible => separatorVisibleObs.value;

  // ********** INITIALISERS **********
  bool test = false;
  bool onlineMode = false;
  Timer tickTimer = Timer(Duration.zero, () => '');
  Timer periodicalTimer = Timer(Duration.zero, () => '');

  // ********** FUNCTIONS **********
  increment() => countObs.value++;

  tick() {
    timeObs.value =
        test ? timeObs.value.add(const Duration(seconds: 1)) : DateTime.now();
    prayerStore.getPrayers(0);
    separatorVisibleObs.value = !separatorVisibleObs.value;
    // print('timer tick ${time.value}');
  }

  periodical() async {
    // update();
    if (kDebugMode) print('periodical start');
    if (kDebugMode) print('ONLINE.VALUE ${apiStore.online.value}');
    if (onlineMode) apiStore.fetchApi();
    if (kDebugMode) print('periodical run finish');
    if (!onlineMode) periodicalTimer.cancel();
  }
}
