// import package resources
// import 'dart:async';
import 'dart:io';
import 'dart:async';
// import 'dart:convert';
import 'package:flutter/foundation.dart';

import 'package:get/get.dart';
// import 'package:http/http.dart' as http;
// import 'dart:convert' as convert;
import 'package:dio/dio.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

// import local package resources
import 'package:prayer_timetable_flutter/store/box.dart';
import 'package:prayer_timetable_flutter/store/class.dart';
import 'package:prayer_timetable_flutter/store/settings.dart';

// import 'package:html_unescape/html_unescape.dart';

// var unescape = HtmlUnescape();

// Instantiate your class using Get.put() to make it available for all 'child' routes there.
final ApiController apiStore = Get.put(ApiController());

class ApiController extends GetxController {
  // ********** LIFECYCLE **********
  @override
  void onInit() async {
    if (kDebugMode) print('api init');
    // fetchApi();
    if (kDebugMode)
      print(
          'hasConnection? ${await InternetConnectionChecker().hasConnection}');

    super.onInit();
  }

  @override
  void onClose() {
    if (kDebugMode) print('api close');
    listener.cancel();
    listenerOn = false;

    super.onClose();
  }

  // ********** OBSERVABLES **********
  var online = false.obs;

  // ********** GETTERS **********

  // ********** INITIALISERS **********

  bool fetchSuccess = false;

  // SETTINGS
  int hijriOffset = DefaultSettings.hijriOffset;
  bool joinMaghrib = DefaultSettings.joinMaghrib;
  bool joinDhuhr = DefaultSettings.joinDhuhr;
  bool jamaahShow = DefaultSettings.jamaahShow;
  List<int> jummuahTime = DefaultSettings.jummuahTime;
  List<int> jummuah2Time = DefaultSettings.jummuah2Time;
  List<int> taraweehTime = DefaultSettings.taraweehTime;
  List<String> jamaahMethods = DefaultSettings.jamaahMethods;
  List<List<int>> jamaahOffsets = DefaultSettings.jamaahOffsets;
  DateTime? updatedSettings = DefaultSettings.updatedSettings;
  bool settingsLoaded = false;
  bool apiLoaded = false;
  bool infoLoaded = false;
  double newsScrollPosition = 0;
  double newsScrollPercentage = 0;
  String info = '';

  // bool connectivity = false;

  Duration durationSinceSettingsUpdated = const Duration(days: 365);
  Duration durationSinceApiUpdated = const Duration(days: 365);
  Map appUpdate = {};
  bool appUpdateShow = false;
  bool appIsLatestBuild = false;

  bool wasOnline = false;
  late StreamSubscription<InternetConnectionStatus> listener;
  bool listenerOn = false;

  // ********** FUNCTIONS **********
  fetchApi() async {
    checkInternet();

    await checkIfWasOnline();
    if (kDebugMode)
      print('wasOnline: ${LocalStorage.settings.get('wasOnline')}');
    // if (wasOnline)
    // if (online.value)
    getApi();

    // HIVE reset
    // LocalStorage.api.clear();
    // LocalStorage.api.deleteFromDisk();
  }

  getApi() async {
    const String _url = 'https://islamireland.ie/api/settings/?new=true';

    // print('stored updatedSettings: ${LocalStorage.settings.get('updatedSettings')}');

    if (!GetPlatform.isWeb) {
      bool _connected = false;
      // check if connected first
      _connected = await InternetConnectionChecker().hasConnection;
      if (!_connected) return;
    }

    try {
      // var response = await http.get(url);
      var response = await Dio().get(_url);
      if (kDebugMode) print('try settings fetch');

      if (response.statusCode == 200) {
        // no need to convert as dio returns Map
        Map<dynamic, dynamic> data = response.data;

        SettingsClass _settings = SettingsClass.fromJson(data);

        fetchSuccess = true;
        hijriOffset = _settings.hijriOffset;
        joinMaghrib = _settings.joinMaghrib;
        joinDhuhr = _settings.joinDhuhr;
        jamaahShow = _settings.jamaahShow;
        jummuahTime = _settings.jummuahTime;
        jummuah2Time = _settings.jummuah2Time;
        taraweehTime = _settings.taraweehTime;
        jamaahMethods = _settings.jamaahMethods;
        jamaahOffsets = _settings.jamaahOffsets;
        updatedSettings = _settings.updatedSettings;

        await LocalStorage.settings.put('hijriOffset', hijriOffset);
        await LocalStorage.settings.put('join', joinMaghrib);
        await LocalStorage.settings.put('joinDhuhr', joinDhuhr);
        await LocalStorage.settings.put('jamaahShow', jamaahShow);
        await LocalStorage.settings.put('jummuahTime', jummuahTime);
        await LocalStorage.settings.put('jummuah2Time', jummuah2Time);
        await LocalStorage.settings.put('taraweehTime', taraweehTime);
        await LocalStorage.settings.put('jamaahMethods', jamaahMethods);
        await LocalStorage.settings.put('jamaahOffsets', jamaahOffsets);
        await LocalStorage.settings.put('updatedSettings', updatedSettings);
        await LocalStorage.settings.put('wasOnline', true);

        fetchSuccess = true;
        settingsLoaded = true;
        if (kDebugMode) print('updatedSettings: $updatedSettings');
      } else {
        settingsLoaded = false;
        if (kDebugMode) print('else: exception');
        // throw Exception('Failed to load post');
      }
    } catch (e) {
      if (e is SocketException) {
        //treat SocketException
        if (kDebugMode) print('Socket exception: ${e.toString()}');
      } else if (e is TimeoutException) {
        //treat TimeoutException
        if (kDebugMode) print('Timeout exception: ${e.toString()}');
      } else if (kDebugMode) print('Unhandled exception: ${e.toString()}');
    }
    if (kDebugMode) print('ended settings fetch');
  }

  Future checkIfWasOnline() async {
    wasOnline = LocalStorage.settings.get('wasOnline') ?? false;
    Future.delayed(const Duration(seconds: 10), () {
      if (!wasOnline) if (kDebugMode)
        print('This app was online at some stage.');
    });
    return true;
  }

  Future checkInternet() async {
    if (kDebugMode) print('started checkInternet');

    listener = InternetConnectionChecker().onStatusChange.listen((status) {
      listenerOn = true;
      switch (status) {
        case InternetConnectionStatus.connected:
          if (kDebugMode) print('Data connection is available.');
          online.value = true;
          // getSettings();
          // update();
          // listener.cancel();
          break;
        case InternetConnectionStatus.disconnected:
          if (kDebugMode) print('You are disconnected from the internet.');
          online.value = false;
          break;
      }
    });
    if (kDebugMode) print('ended checkInternet');

    return true;
  }
}
