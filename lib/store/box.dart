import 'package:hive/hive.dart';

@HiveType(typeId: 1)
class Settings {
  @HiveField(0)
  String name = '';

  @HiveField(1)
  int age = 0;

  @HiveField(2)
  List<String> friends = [''];

  @override
  String toString() {
    return '$name: $age';
  }
}

class LocalStorage {
  //static Box prefs = Hive.box('prefs');
  static Box settings = Hive.box('settings');
  static Box log = Hive.box('log');
}
