// import package resources
// import 'dart:async';
import 'dart:async';
// import 'dart:convert';
import 'package:flutter/foundation.dart';

import 'package:get/get.dart';
// import 'package:http/http.dart' as http;
// import 'dart:convert' as convert;
import 'package:internet_connection_checker/internet_connection_checker.dart';

// import local package resources
import 'package:prayer_timetable_flutter/store/box.dart';
import 'package:prayer_timetable_flutter/store/class.dart';

// import 'package:html_unescape/html_unescape.dart';

// var unescape = HtmlUnescape();

// Instantiate your class using Get.put() to make it available for all 'child' routes there.
final SettingsController settingsStore = Get.put(SettingsController());

class SettingsController extends GetxController {
  // ********** LIFECYCLE **********
  @override
  void onInit() async {
    if (kDebugMode) print('settings store init');
    // getLocal();

    // HIVE reset
    // LocalStorage.api.clear();
    // LocalStorage.api.deleteFromDisk();

    super.onInit();
  }

  @override
  void onClose() {
    if (kDebugMode) print('settings store close');

    super.onClose();
  }

  // ********** OBSERVABLES **********

  // ********** GETTERS **********

  // ********** INITIALISERS **********

  // SETTINGS
  String title = DefaultSettings.title;
  bool onlineMode = DefaultSettings.onlineMode;
  bool log = DefaultSettings.log;
  bool jamaahShow = DefaultSettings.jamaahShow;
  bool jamaahOverlay = DefaultSettings.jamaahOverlay;
  bool joinMaghrib = DefaultSettings.joinMaghrib;
  bool joinDhuhr = DefaultSettings.joinDhuhr;

  int updateInterval = DefaultSettings.updateInterval;
  int hijriOffset = DefaultSettings.hijriOffset;

  List<int> jummuahTime = DefaultSettings.jummuahTime;
  List<int> jummuah2Time = DefaultSettings.jummuah2Time;
  List<int> taraweehTime = DefaultSettings.taraweehTime;
  List<String> jamaahMethods = DefaultSettings.jamaahMethods;
  List<List<int>> jamaahOffsets = DefaultSettings.jamaahOffsets;

  DateTime? updatedSettings =
      DateTime.now().subtract(const Duration(days: 365));
  DateTime? updatedApi = DateTime.now().subtract(const Duration(days: 365));

  bool settingsLoaded = false;
  bool apiLoaded = false;

  // bool connectivity = false;

  Duration durationSinceSettingsUpdated = const Duration(days: 365);
  Duration durationSinceApiUpdated = const Duration(days: 365);
  Map appUpdate = {};
  bool appUpdateShow = false;
  bool appIsLatestBuild = false;

  bool wasOnline = false;
  late StreamSubscription<InternetConnectionStatus> listener;
  bool listenerOn = false;

  // ********** FUNCTIONS **********
  getLocal() {
    // settings
    hijriOffset = LocalStorage.settings.get('hijriOffset') ?? hijriOffset;
    joinMaghrib = LocalStorage.settings.get('joinMaghrib') ?? joinMaghrib;
    joinDhuhr = LocalStorage.settings.get('joinDhuhr') ?? joinDhuhr;
    jamaahShow = LocalStorage.settings.get('jamaahShow') ?? jamaahShow;
    jummuahTime = LocalStorage.settings
            .get('jummuahTime', defaultValue: jummuahTime)
            .cast<int>() ??
        jummuahTime;
    jummuah2Time = LocalStorage.settings
            .get('jummuah2Time', defaultValue: jummuah2Time)
            .cast<int>() ??
        jummuah2Time;
    taraweehTime = LocalStorage.settings
            .get('taraweehTime', defaultValue: taraweehTime)
            .cast<int>() ??
        taraweehTime;
    jamaahMethods = LocalStorage.settings
            .get('jamaahMethods', defaultValue: jamaahMethods)
            .cast<String>() ??
        jamaahMethods;

    // print(LocalStorage.settings.get('jamaahMethods').runtimeType);
    // print('***vs***');
    // print(jamaahMethods.runtimeType);

    if (LocalStorage.settings.get('jamaahOffsets') != null)
      List<List<int>>.from(LocalStorage.settings.get('jamaahOffsets'));
    else
      jamaahOffsets = jamaahOffsets;

    // .where((i) => i.flag == true));
    // .cast<List<List<int>>(); //?? jamaahOffsets;
    updatedSettings =
        LocalStorage.settings.get('updatedSettings') ?? updatedSettings;
  }
}
