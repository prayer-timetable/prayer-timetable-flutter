// import 'package:prayer_timetable/prayer_timetable.dart';

class Strings {
  final String time;
  final String date;
  final String hijri;
  final String hijriMonth;
  final String countdownName;
  final String countdownTime;
  final String countupName;
  final String countupNameFrom;
  final String countupTime;
  final List<String> prayers;
  final List<String> jamaah;
  final String current;
  final String updated;
  final String midnight;
  final String lastThird;
  final String nextName;
  final String when;
  final String hijriMonthTrue;
  final String todayDate;
  final String todayHijri;
  String appName;
  String appVersion;
  String appPackageName;
  String appBuildNumber;
  List<String> prayerNames;

  Strings(
      {this.time = ' ',
      this.date = ' ',
      this.hijri = ' ',
      this.hijriMonth = '',
      this.countdownName = ' ',
      this.countdownTime = ' ',
      this.countupName = ' ',
      this.countupNameFrom = ' ',
      this.countupTime = ' ',
      this.prayers = const ['', '', '', '', '', ''],
      this.jamaah = const ['', '', '', '', '', ''],
      this.current = '',
      this.updated = '',
      this.midnight = '',
      this.lastThird = '',
      this.nextName = '',
      this.when = '',
      this.hijriMonthTrue = '',
      this.todayDate = '',
      this.todayHijri = '',
      this.appName = '',
      this.appVersion = '',
      this.appPackageName = '',
      this.appBuildNumber = '',
      this.prayerNames = const [
        'fajr',
        'shurooq',
        'dhuhr',
        'asr',
        'maghrib',
        'isha'
      ]});
}

Strings defaultStrings = Strings(
  time: '00',
  // now: '',
  // date: '',
  // hijri: '',
  // hijriMonth: '',
  // countdownName: '',
  // countdownTime: '',
  // countupName: '',
  // countupTime: '',
  // countupNameFrom: '',
  // prayers: ['', '', '', '', '', ''],
  // jamaah: ['', '', '', '', '', ''],
  // current: '',
  // midnight: '',
  // lastThird: '',
  // nextName: '',
  // when: '',
  // hijriMonthTrue: '',
  // todayDate: '',
  // todayHijri: '',
  appName: '',
  appVersion: '',
  appPackageName: '',
  appBuildNumber: '',
);

// Calc defauldCalc = Calc(DateTime _time: DateTime.now(), Prayers* prayersToday, Prayers* prayersTomorrow, Prayers* prayersYesterday, {bool* jamaahOn = false, Prayers* jamaahToday, Prayers* jamaahTomorrow, Prayers* jamaahYesterday})

// {title: Islamic Cultural Centre of Ireland, hijriOffset: 0,
// joinMaghrib: false, joinDhuhr: false, noJamaah: false,
// jummuahTime: [13, 15], jummuahTime2: [0, 0], jummuahMinutes: 45, taraweehTime: [22, 0],
// jamaahMethods: [afterthis, , fixed, afterthis, afterthis, afterthis],
// jamaahOffsets: [[0, 20], [0, 0], [13, 50], [0, 10], [0, 10], [0, 5]],
// updatedSettings: 1648918750}
class SettingsClass {
  String title;

  bool onlineMode;
  bool log;
  bool jamaahShow;
  bool jamaahOverlay;
  bool joinMaghrib;
  bool joinDhuhr;

  int updateInterval;
  int hijriOffset;

  List<int> jummuahTime;
  List<int> jummuah2Time;
  final List<int> taraweehTime;
  final List<String> jamaahMethods;
  final List<List<int>> jamaahOffsets;
  DateTime? updatedSettings =
      DateTime.now().subtract(const Duration(days: 365));
  DateTime? updatedApi = DateTime.now().subtract(const Duration(days: 365));

  SettingsClass({
    this.title = '',
    this.onlineMode = false,
    this.log = false,
    this.jamaahShow = true,
    this.jamaahOverlay = true,
    this.joinMaghrib = false,
    this.joinDhuhr = false,
    this.updateInterval = 60,
    this.hijriOffset = 0,
    this.jummuahTime = const [13, 10],
    this.jummuah2Time = const [13, 10],
    this.taraweehTime = const [22, 0],
    this.jamaahMethods = const [
      'afterthis',
      '',
      'afterthis',
      'afterthis',
      'afterthis',
      'afterthis'
    ],
    this.jamaahOffsets = const [
      [0, 20],
      [0, 0],
      [0, 5],
      [0, 5],
      [0, 5],
      [0, 5]
    ],
    this.updatedSettings,
    this.updatedApi,
  });

  factory SettingsClass.fromJson(Map<dynamic, dynamic> parsedJson) {
    // print('parsedJson: $parsedJson');
    // print('hijriOffset: ${parsedJson['hijriOffset'].runtimeType}');
    // print('joinDhuhr: ${parsedJson['joinDhuhr'].runtimeType}');
    // print('noJamaah: ${parsedJson['noJamaah'].runtimeType}');
    // print('jummuahTime: ${parsedJson['jummuahTime'].runtimeType}');
    // print('jummuahTime2: ${parsedJson['jummuahTime2'].runtimeType}');
    // print('taraweehTime: ${parsedJson['taraweehTime'].runtimeType}');
    // print('jamaahMethods: ${parsedJson['jamaahMethods'].runtimeType}');
    // print('jamaahOffsets: ${parsedJson['jamaahOffsets'].runtimeType}');
    // print('updatedSettings: ${parsedJson['updatedSettings'].runtimeType}');

    return SettingsClass(
      hijriOffset: parsedJson['hijriOffset'],
      joinMaghrib: parsedJson['joinMaghrib'],
      joinDhuhr: parsedJson['joinDhuhr'],
      jamaahShow: parsedJson['jamaahShow'],
      jummuahTime: [
        parsedJson['jummuahTime'][0],
        parsedJson['jummuahTime'][1],
      ],
      jummuah2Time: [
        parsedJson['jummuah2Time'][0],
        parsedJson['jummuah2Time'][1],
      ],
      taraweehTime: [
        parsedJson['taraweehTime'][0],
        parsedJson['taraweehTime'][1],
      ],
      jamaahMethods: [
        parsedJson['jamaahMethods'][0],
        parsedJson['jamaahMethods'][1],
        parsedJson['jamaahMethods'][2],
        parsedJson['jamaahMethods'][3],
        parsedJson['jamaahMethods'][4],
        parsedJson['jamaahMethods'][5],
      ],
      jamaahOffsets: [
        [parsedJson['jamaahOffsets'][0][0], parsedJson['jamaahOffsets'][0][1]],
        [0, 0],
        [parsedJson['jamaahOffsets'][2][0], parsedJson['jamaahOffsets'][2][1]],
        [parsedJson['jamaahOffsets'][3][0], parsedJson['jamaahOffsets'][3][1]],
        [parsedJson['jamaahOffsets'][4][0], parsedJson['jamaahOffsets'][4][1]],
        [parsedJson['jamaahOffsets'][5][0], parsedJson['jamaahOffsets'][5][1]],
      ],
      updatedSettings: DateTime.fromMillisecondsSinceEpoch(
          (parsedJson['updatedSettings']) * 1000),
    );
  }
}

class DefaultSettings {
  static const String title = '';

  static const bool onlineMode = false;
  static const bool log = false;
  static const bool joinMaghrib = false;
  static const bool joinDhuhr = false;
  static const bool jamaahShow = false;
  static const bool jamaahOverlay = true;

  static const int hijriOffset = 0;
  static const int updateInterval = 60;

  static final List<int> jummuahTime = [13, 10];
  static final List<int> jummuah2Time = [14, 30];
  static final List<int> taraweehTime = [14, 30];

  static final List<String> jamaahMethods = [
    'afterthis',
    '',
    'afterthis',
    'afterthis',
    'afterthis',
    'afterthis'
  ];
  static final List<List<int>> jamaahOffsets = [
    [0, 30],
    [],
    [0, 5],
    [0, 5],
    [0, 5],
    [0, 5]
  ];

  static final DateTime updatedSettings =
      DateTime.now().subtract(const Duration(days: 365));

  static final DateTime updatedApi =
      DateTime.now().subtract(const Duration(days: 365));
  // bool this.join = 0;
}
