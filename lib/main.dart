import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

import 'package:desktop_window/desktop_window.dart';
// import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';

// import 'package:prayer_timetable_flutter/store/class.dart';
import 'package:prayer_timetable_flutter/theme/theme.dart';
// import 'package:prayer_timetable_flutter/store/state.dart';

import 'package:prayer_timetable_flutter/components/Main.dart';

// import 'package:prayer_timetable_flutter/components/prayer.dart';

void main() async {
  // needed for tz?, desktopwindow, notifications and workmanager
  WidgetsFlutterBinding.ensureInitialized();

  if (!GetPlatform.isWeb) await windowFunctions();

  await Hive.initFlutter();
  // await Hive.openBox('prefs');
  // await Hive.openBox<SettingsClass>('settings');
  await Hive.openBox('settings');
  await Hive.openBox('log');
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(context) {
    // Instantiate your class using Get.put() to make it available for all "child" routes there.
    // final MainController mc = Get.put(MainController());

    // print(stateStore.count);

    // without screen util
    // return MaterialApp(
    //     title: 'Flutter Demo',
    //     theme: theme,
    //     home: Scaffold(
    //       // Replace the 8 lines Navigator.push by a simple Get.to(). You don't need context
    //       body: Main(),
    //     ));
    // with screen util
    return ScreenUtilInit(
        designSize: const Size(1080, 1920),
        // designSize: const Size(1280, 720),
        minTextAdapt: true,
        // splitScreenMode: true,
        builder: (_) {
          return MaterialApp(
              title: 'Flutter Demo',
              theme: theme,
              home: const Scaffold(
                // Replace the 8 lines Navigator.push by a simple Get.to(). You don't need context
                body: Main(),
              ));
        });

    // end
  }
}

// class Other extends StatelessWidget {
//   // You can ask Get to find a Controller that is being used by another page and redirect you to it.
//   final MainController mc = Get.find();

//   @override
//   Widget build(context) {
//     // Access the updated count variable
//     return Obx(() =>Text("${mc.count}"));
//   }
// }

Future windowFunctions() async {
  // Size size = await DesktopWindow.getWindowSize();
  // print(size);
  if (kReleaseMode) {
    // or kDebugMode
    if (kDebugMode) print('already controlled via linux my_application.cc');

    // await DesktopWindow.setWindowSize(const Size(1920, 1080));
    // await DesktopWindow.setMinWindowSize(const Size(1920, 1080));
    // await DesktopWindow.setMaxWindowSize(const Size(1920, 1080));
    // await DesktopWindow.toggleFullScreen(); //***this
    // await DesktopWindow.setFullScreen(true); //***this
  }
  if (GetPlatform.isMacOS) {
    await DesktopWindow.setWindowSize(const Size(720, 1280));
    await DesktopWindow.setMinWindowSize(const Size(720, 1280));
    await DesktopWindow.setMaxWindowSize(const Size(720, 1280));
  }
  // await DesktopWindow.resetMaxWindowSize();
  // bool isFullScreen = await DesktopWindow.getFullScreen();
  // print(isFullScreen);
  // await DesktopWindow.setFullScreen(false);
}
